import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Hero} from './../hero';
import { HeroDetailComponent } from './../hero-detail/hero-detail.component';
import {HeroService} from './../service/hero.service';


@Component({
    selector: 'my-heros',
    templateUrl: "app/hero-list/heroes.component.html",
    styleUrls: ['app/hero-list/heroes.component.css'],
    directives: [HeroDetailComponent],
    providers: []
})

export class HeroesComponent implements OnInit {
    heroes:Hero[];
    selectedHero:Hero;
    addingHero = false;
    error: any;

    constructor(
        private heroService:HeroService,
        private router:Router) {
    }

    getHeroes() {
        this.heroService
            .getHeroes()
            .then(heroes => this.heroes = heroes)
            .catch(error => this.error = error);
    }


    onSelect(hero:Hero) {
        this.selectedHero = hero;
        this.addingHero = false;
    }


    ngOnInit() {
        this.getHeroes();
    }

    gotoDetail() {
        let link = ['/detail', this.selectedHero.id];
        this.router.navigate(link);
    }


    addHero() {
        this.addingHero = true;
        this.selectedHero = null;
    }

    close(savedHero: Hero) {
        this.addingHero = false;
        if (savedHero) { this.getHeroes(); }
    }


    deleteHero(hero: Hero, event: any) {
        event.stopPropagation();
        this.heroService
            .delete(hero)
            .then(res => {
                this.heroes = this.heroes.filter(h => h !== hero);
                if (this.selectedHero === hero) { this.selectedHero = null; }
            })
            .catch(error => this.error = error);
    }
}